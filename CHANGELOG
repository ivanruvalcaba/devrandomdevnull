2020-01-25  Iván Ruvalcaba  <ivanruvalcaba[at]disroot[dot]org>

    * robots.txt (Sitemap): Added URL.
    * _config.txt (twitter_username, rss): Added my Twitter profile.
    (rss): Update text description.
    (google_analytics): Added Google ID.

2020-01-24  Iván Ruvalcaba  <ivanruvalcaba[at]disroot[dot]org>

    * README.markdown: Rename file from README.md.

2020-01-23  Iván Ruvalcaba  <ivanruvalcaba[at]disroot[dot]org>

    * README.md: Added project URL.

2020-01-23  Iván Ruvalcaba  <ivanruvalcaba[at]disroot[dot]org>

    * Fix GitLab Page assets issue.
        + _config.yml (url, baseurl): Set project domain and subdomain
        URL.
        + head.html: Fix relative URL issues.

2020-01-23  Iván Ruvalcaba  <ivanruvalcaba[at]disroot[dot]org>

    * Initial commit.
        + .gitignore (windows, linux, vim): New file types excluded.
        + _config.yml (gpg_fingerprint): New variable.
        (show_excerpts): Set variable to true.
        (header_pages): Define acerca-de as new navbar item.
        (jekyll-email-protect, jekyll-email-protect, jekyll-gist,
        jekyll-sitemap, jemoji, premonition): Added new Jekyll plugins.
        (disqus shortname): Set Disqus shortname.
        (gitlab_usename): New variable.
        (keybase_usename): New variable.
        (permalink): Set variable to :title.
        (jekyll-archives): Set plugin variables.
        + footer.html: New custom website footer.
            + Added GnuPG fingerprint if site.email is defined.
            + Added website license and legal notice.
        + LICENSE: Added project license.
        + CHANGELOG: Added project changelog.
        + robots.txt (*.gif, *.jpg, *.jpeg, *.png, *.svg, *.webp):
        Disallow crawling of all website images.
        + humans.txt: Added project and author details.
        + Gemfile (jekyll-archive, jekyll-email-protect, jekyll-gist,
        jekyll-sitemap, jemoji, premonition): Added new Jekyll plugins.
        + assets (android-chrome-192x192.png,
        android-chrome-512x512.png, apple-touch-icon.png,
        browserconfig.xml, favicon-16x16.png, favicon-32x32.png,
        mstile-150x150.png, site.webmanifest): Added website favicons.
        (premonition.css): Added Jekyll premonition plugin stylesheet.
        + favicon.ico: Added website favicon.
        + head.html: New custom website head.
            + Added Jekyll premonition plugin stylesheet.
            + Added Font Awesome 4.7.0 assets (requided by pemonition).
            + Added website favicon's assets.
            + (dcterms.date): Added meta tag.
            + Added humans.txt reference.
        + home.html: New custom website home.
        + .gitlab-ci.yml: Added GitLab-CI config file. See:
        (https://gitlab.com/pages/jekyll).
        + social.html: Added Keybase, GitLab social media favicons.
        + minima-social-icons.svg: Added folder, tag, Keybase and GitLab
        sprite favicons.
        + README.md: Add README to project.
        + archive.html: Create website archive layout.
        + post.html: Added post tags and categories anchors.
