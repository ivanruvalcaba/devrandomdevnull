[//]: # (Filename: README.md)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <ivanruvalcaba[at]disroot[dot]org>)
[//]: # (Created: 21 ene 2020 11:12:55)
[//]: # (Last Modified: 23 ene 2020 21:44:10)

# $ cat /dev/random > /dev/null 2>&1

Archivos fuente de [$ cat /dev/random > /dev/null 2>&1](https://ivanruvalcaba.gitlab.io/devrandomdevnull/).

## Descripción

Este sitio web es construido a través de [Jekyll](https://jekyllrb.com/), impulsado por [GitLab CI](https://about.gitlab.com/gitlab-ci/) y hospedado en [GitLab Pages](https://pages.gitlab.io/).

Consulte la documentación del siguiente [repositorio](https://gitlab.com/pages/jekyll) para mayor información.

## Contacto

Si desea contribuir al proyecto o contactar a su autor, por favor refiérase a: [ivanruvalcaba\[at\]disroot\[dot\]org](mailto:ivanruvalcaba@disroot.org?subject=[devrandomdevnull]_Feedback).

## Licencia

Copyright (c) 2020 — Iván Ruvalcaba. _A no ser que se indique explícitamente lo contrario, todo el contenido de este sitio web se encuentra sujeto bajo los términos de la licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional (CC BY-SA 4.0 International)](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES)"._
